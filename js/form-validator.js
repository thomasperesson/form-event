// Déclaration des sélecteur du formulaire
const nameInput = document.getElementById("name");
const emailInput = document.getElementById("email");
const phoneInput = document.getElementById("phone");
const submitButton = document.getElementById("submit");

// Couleur text / bordures
const warningColor = "red";
const successColor = "green";

// Déclaration de la variable error
let error = true;

// Déclaration des éléments quand il y a une erreur
let errorNameText =
    "Ce champ ne doit pas être vide ou dépasser les 50 caractères";
const spanErrorName = document.createElement("span");
spanErrorName.setAttribute("id", "errorSpanName");
spanErrorName.innerHTML = errorNameText;
spanErrorName.style.color = warningColor;
spanErrorName.style.padding = "5px 0 5px 20px";
spanErrorName.style.fontSize = "12px";
spanErrorName.style.display = "none";
nameInput.parentElement.appendChild(spanErrorName);

let errorEmailText =
    "Ce champ ne doit pas être vide ou doit être valide &laquo; exemple-email@e-mail.com &raquo;";
const spanErrorEmail = document.createElement("span");
spanErrorEmail.innerHTML = errorEmailText;
spanErrorEmail.style.color = warningColor;
spanErrorEmail.style.padding = "5px 0 5px 20px";
spanErrorEmail.style.fontSize = "12px";
spanErrorEmail.style.display = "none";
emailInput.parentElement.appendChild(spanErrorEmail);

let errorPhoneText =
    "Ce champ ne doit pas être vide ou doit être valide &laquo; 0600000000 &raquo;";
const spanErrorPhone = document.createElement("span");
spanErrorPhone.innerHTML = errorPhoneText;
spanErrorPhone.style.color = warningColor;
spanErrorPhone.style.padding = "5px 0 5px 20px";
spanErrorPhone.style.fontSize = "12px";
spanErrorPhone.style.display = "none";
phoneInput.parentElement.appendChild(spanErrorPhone);

// Déclaration des Regex pour le Prénom et l'Email
const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
const phoneRegex = /^(0)[6|7][0-9]{8}$/;

// Élément caractères restants
const spanCharRest = document.createElement("span");
nameInput.parentElement.style.position = "relative";
spanCharRest.style.position = "absolute";
spanCharRest.style.right = "10px";
spanCharRest.style.bottom = "0px";
spanCharRest.style.transform = "translateY(-50%)";
spanCharRest.innerText = "50";
nameInput.parentElement.appendChild(spanCharRest);

/**
 * Vérification du champ Prénom et affichage des erreurs
 */
const nameValidation = () => {
    nameInput.addEventListener("keyup", (e) => {
        let charRest = `${50 - e.target.value.length}`;
        if (charRest <= 0) {
            charRest = 0;
        }
        spanCharRest.innerText = charRest;
    });
    nameInput.maxLength = 50;
    nameInput.addEventListener("keyup", (e) => {
        if (e.target.value.length <= 0 || e.target.value.length > 50) {
            nameInput.style.border = `1px solid ${warningColor}`;
            spanErrorName.style.display = "block";
            spanCharRest.style.bottom = "31px";
            error = true;
        } else {
            nameInput.style.border = `1px solid ${successColor}`;
            spanErrorName.style.display = "none";
            spanCharRest.style.bottom = "0px";
            error = false;
        }
    });
    return error;
};

/**
 * Vérification du champ Email et affichage des erreurs
 */
const emailValidation = () => {
    emailInput.addEventListener("keyup", (e) => {
        if (
            e.target.value.length <= 0 ||
            !emailRegex.test(e.target.value.toLowerCase())
        ) {
            emailInput.style.border = `1px solid ${warningColor}`;
            spanErrorEmail.style.display = "block";
            error = true;
        } else {
            emailInput.style.border = `1px solid ${successColor}`;
            spanErrorEmail.style.display = "none";
            error = false;
        }
    });
    return error;
};

/**
 * Vérification du champ Téléphone et affiche des erreurs
 */
const phoneValidation = () => {
    phoneInput.addEventListener("keyup", (e) => {
        let inputNumberValue = e.target.value.replace(/\-/g, "");
        inputNumberValue = inputNumberValue.replace(/ /g, "");
        inputNumberValue = inputNumberValue.replace(/\//g, "");
        if (e.target.length <= 0 || !phoneRegex.test(inputNumberValue)) {
            phoneInput.style.border = `1px solid ${warningColor}`;
            spanErrorPhone.style.display = "block";
            error = true;
        } else {
            phoneInput.style.border = `1px solid ${successColor}`;
            spanErrorPhone.style.display = "none";
            error = false;
        }
    });
    return error;
};

const nameChecking = () => {
    if (nameInput.value.length <= 0 || nameInput.value.length > 50) {
        nameInput.style.border = `1px solid ${warningColor}`;
        spanErrorName.style.display = "block";
        spanCharRest.style.bottom = "31px";
        error = true;
    } else {
        nameInput.style.border = `1px solid ${successColor}`;
        spanErrorName.style.display = "none";
        spanCharRest.style.bottom = "0px";
        error = false;
    }
};

const emailChecking = () => {
    if (
        emailInput.value.length <= 0 ||
        !emailRegex.test(emailInput.value.toLowerCase())
    ) {
        emailInput.style.border = `1px solid ${warningColor}`;
        spanErrorEmail.style.display = "block";
        error = true;
    } else {
        emailInput.style.border = `1px solid ${successColor}`;
        spanErrorEmail.style.display = "none";
        error = false;
    }
};

const phoneChecking = () => {
    if (phoneInput.length <= 0 || !phoneRegex.test(phoneInput.value)) {
        phoneInput.style.border = `1px solid ${warningColor}`;
        spanErrorPhone.style.display = "block";
        error = true;
    } else {
        phoneInput.style.border = `1px solid ${successColor}`;
        spanErrorPhone.style.display = "none";
        error = false;
    }
};

submitButton.addEventListener("click", (e) => {
    if (nameValidation() || emailValidation() || phoneValidation()) {
        nameChecking();
        emailChecking();
        phoneChecking();
        e.preventDefault();
    }
});
nameValidation();
emailValidation();
phoneValidation();
